<?php
define( 'PHOTOSWIPE_ELEMENTS_CLASS_NAME', 'photoswipe-img' );

add_action( 'wp_enqueue_scripts', 'factorysnc_enqueue_assets' );

function factorysnc_enqueue_assets() {
	/* CSS Register */
	wp_register_style( 'animsition-css', 'https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css' );
	wp_register_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' );
	wp_register_style( 'font-awesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_register_style( 'factorysnc-jarallax-css', get_template_directory_uri() . '/assets/css/factorysnc-jarallax.css' );
	wp_register_style( 'wordpress-editor-alignment-classes-css', get_template_directory_uri() . '/assets/css/wordpress-editor-alignment-classes.css' );
	wp_register_style( 'photoswipe-css', get_template_directory_uri() . '/inc/photoswipe/photoswipe.css', array( 'photoswipe-default-skin-css' ) );
	wp_register_style( 'photoswipe-default-skin-css', get_template_directory_uri() . '/inc/photoswipe/default-skin/default-skin.css' );
	wp_register_style( 'swiper-css', get_template_directory_uri() . '/assets/css/swiper.min.css' );
	wp_register_style( 'jssocials-css', "https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" );
	wp_register_style( 'jssocials-flat-css', "https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css", array( 'jssocials-css' ) );
	wp_register_style( 'factorysnc-css', get_stylesheet_uri(), array( 'bootstrap-css', 'animsition-css', 'font-awesome-css', 'factorysnc-jarallax-css', 'wordpress-editor-alignment-classes-css', 'swiper-css', 'jssocials-flat-css' ) );

	/* JS Register */
	wp_register_script( 'animsition-js', 'https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', array( 'jquery', 'popper-js' ), false, true );
	wp_register_script( 'factorysnc-page-transition-js', get_template_directory_uri() . '/assets/js/factorysnc-page-transition.js', array( 'animsition-js' ), false, true );
	wp_register_script( 'jarallax-js', get_template_directory_uri() . '/assets/js/jarallax.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'swiper-js', get_template_directory_uri() . '/assets/js/swiper.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'jssocials-js', "https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js", array( 'jquery' ), false, true );
	wp_register_script( 'isjs-js', get_template_directory_uri() . '/assets/js/is.min.js', array( 'jquery' ), false, true );
	wp_register_script( 'jquery-touch-events-js', "https://cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/1.0.9/jquery.mobile-events.js", array( 'jquery' ), false, true );
	wp_register_script( 'factorysnc-js', get_template_directory_uri() . '/assets/js/global.js', array( 'jquery', 'bootstrap-js', 'factorysnc-page-transition-js', 'swiper-js', 'jssocials-js', 'isjs-js', 'jquery-touch-events-js' ), false, true );


	wp_register_script( 'factorysnc-photoswipe-js', get_template_directory_uri() . '/assets/js/factorysnc-photoswipe.js', array( 'photoswipe-js' ), false, true );
	wp_register_script( 'photoswipe-js', get_template_directory_uri() . '/inc/photoswipe/photoswipe.min.js', array( 'jquery', 'photoswipe-ui-default-js' ), false, true );
	wp_register_script( 'photoswipe-ui-default-js', get_template_directory_uri() . '/inc/photoswipe/photoswipe-ui-default.js', array( 'jquery' ), false, true );

	if ( is_page() || is_single() ) {
		wp_enqueue_script( 'factorysnc-photoswipe-js' );
	}

	if ( wp_script_is( 'factorysnc-photoswipe-js', 'enqueued' ) ) {
		wp_enqueue_style( 'photoswipe-css' );
	}

	/* CSS Enqueue */
	wp_enqueue_style( 'factorysnc-css' );
	/* JS Enqueue */
	wp_enqueue_script( 'factorysnc-js' );

}

add_action( 'admin_enqueue_scripts', 'factorysnc_enqueue_admin_assets' );

function factorysnc_enqueue_admin_assets() {
	global $pagenow;

	/* CSS Register */
	wp_register_style( 'factorysnc-custom-wordpress-media-library-css', get_template_directory_uri() . '/assets/css/factorysnc-custom-wordpress-media-library.css' );
	/* JS Register */
	wp_register_script( 'factorysnc-custom-wordpress-media-library-js' , get_template_directory_uri() . '/assets/js/factorysnc-custom-wordpress-media-library.js' , array( 'jquery' , 'imagesloaded-js' ) , false);
	wp_register_script( 'imagesloaded-js' , 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js' , array( 'jquery' ) , false , true );
	wp_register_script( 'factorysnc-banner-with-text-js' , get_template_directory_uri() . '/assets/js/factorysnc-banner-with-text.js' , array( 'jquery' ) );
	// wp_register_script( 'clipboard-js' , 'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js' , array( 'jquery' ) , false , true );


	if ( $pagenow == 'post.php' && ( get_post_type() == 'page' || get_post_type() == 'post' )) {
		/* CSS Enqueue */
		wp_enqueue_style( 'factorysnc-custom-wordpress-media-library-css' );
		/* JS Enqueue */
		wp_enqueue_script( 'factorysnc-custom-wordpress-media-library-js' );
		wp_enqueue_script( 'factorysnc-banner-with-text-js' );
		// wp_enqueue_script( 'clipboard-js' );
	}
}

add_action( 'after_setup_theme', 'factorysnc_theme_setup' );

function factorysnc_theme_setup() {
	register_nav_menus( array(
		'primary' => 'Primary Menu',
	) );
	add_theme_support( 'post-thumbnails' );

	/*// Add other useful image sizes for use through Add Media modal
	add_image_size( 'medium-width', 480 );
	add_image_size( 'medium-height', 9999, 480 );
	add_image_size( 'medium-something', 480, 480 );*/

	$defaults = array(
		'height' => 100,
		'width' => 400,
		'flex-height' => true,
		'flex-width' => true,
		'header-text' => array( 'site-title', 'site-description' ),
	);

	add_theme_support( 'custom-logo', $defaults );
	add_post_type_support( 'page', 'excerpt' );
}

/*// Register the three useful image sizes for use in Add Media modal
add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
function wpshout_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'medium-width' => __( 'Medium Width' ),
        'medium-height' => __( 'Medium Height' ),
        'medium-something' => __( 'Medium Something' ),
    ) );
}*/

add_filter( 'attachment_fields_to_edit', 'remove_media_link', 10, 2 );

function remove_media_link( $form_fields, $post ) {
	unset( $form_fields[ 'url' ] );
	return $form_fields;
}

add_action( 'template_redirect', 'wpa_attachment_redirect' );

function wpa_attachment_redirect( $post ) {
	if ( is_attachment() ) {
		wp_redirect( get_the_guid() );
	}
}

add_filter( 'clean_url', 'load_async_scripts', 11, 1 );

function load_async_scripts( $url ) {
	if ( strpos( $url, '#asyncload' ) === false )
		return $url;
	else if ( is_admin() )
		return str_replace( '#asyncload', '', $url );
	else
		return str_replace( '#asyncload', '', $url ) . "' async";
}

add_filter( 'clean_url', 'load_defer_scripts', 11, 1 );

function load_defer_scripts( $url ) {
	if ( strpos( $url, '#deferload' ) === false )
		return $url;
	else if ( is_admin() )
		return str_replace( '#deferload', '', $url );
	else
		return str_replace( '#asyncload', '', $url ) . "' defer";
}

//init the meta box
add_action( 'after_setup_theme', 'add_custom_metaboxes' );

function add_custom_metaboxes() {
	//custom_wordpress_media_library_meta_box
	add_action( 'add_meta_boxes' , 'custom_wordpress_media_library_meta_box' );
	add_action( 'save_post' , 'custom_wordpress_media_library_meta_box_save' , 10 , 1 );

	//factorysnc_banner_with_text_meta_box
	add_action( 'add_meta_boxes' , 'factorysnc_banner_with_text_meta_box' );
}

function factorysnc_banner_with_text_meta_box(){
	$type = array( 'post' , 'page' );
	foreach ($type as $t) {
		add_meta_box( 'factorysnc_banner_with_text_meta_box', __( 'Banner - Genera ShortCode', 'factorysnc'), 'factorysnc_banner_with_text_meta_box_callback', $t, 'side', 'default', null );
	}
}

function factorysnc_banner_with_text_meta_box_callback($post){
	include( 'assets/php/factorysnc-banner-with-text.php' );
}

function custom_wordpress_media_library_meta_box(){
	$type = array( 'post' , 'page' );
	foreach ($type as $t) {
		add_meta_box( 'custom_wordpress_media_library_meta_box', __( 'Gallery Fotografica', 'factorysnc'), 'custom_wordpress_media_library_meta_box_callback', $t, 'normal', 'default', null );
	}
}

function custom_wordpress_media_library_meta_box_callback( $post ) {
	$meta_images = get_post_meta( $post->ID, '_gallery_images', true );
	include( 'assets/php/factorysnc-custom-wordpress-media-library.php' );
}

function custom_wordpress_media_library_meta_box_save( $post_id ) {
	if ( !current_user_can( 'edit_posts', $post_id ) ) {
		return 'not permitted';
	}
	if ( isset( $_POST[ 'custom_wordpress_media_library_meta_box_nonce' ] ) && wp_verify_nonce( $_POST[ 'custom_wordpress_media_library_meta_box_nonce' ], 'custom_wordpress_media_library_meta_box' ) ) {
		$post_key = 'gallery_images';
		$images = '';

		if ( isset( $_POST[ $post_key ] ) ) {
			$images = $_POST[ $post_key ];
			$images = trim( $images );
			$images = stripslashes( $images );
			$images = htmlspecialchars( $images );
		}
		update_post_meta( $post_id, '_' . $post_key, $images );
	}
}

add_filter( 'get_the_excerpt', 'factorysnc_get_the_excerpt' );

function factorysnc_get_the_excerpt( $excerpt ) {

	$post = get_post();

	if ( $post->post_excerpt != "" ) {
		$output = $excerpt;
	} else {
		$output = '';
	}

	return $output;
}

function factorysnc_get_gallery_images_full() {
	return get_gallery_images( 'full' );
}

function factorysnc_get_gallery_images( $image_size ) {
	$items = array();
	$meta_images = get_post_meta( get_the_ID(), '_gallery_images', true );
	if ( strpos( $meta_images, ',' ) !== false ) {
		$gallery_images = explode( ',', $meta_images );
	} else {
		$gallery_images = $meta_images;
	}
	$index = 0;
	if ( $meta_images != "" ) {
		foreach ( $gallery_images as $gallery_image ) {
			$id = $gallery_image;
			$image_data = wp_get_attachment_image_src( $id, $image_size );
			$item = array(
				'src' => $image_data[ 0 ],
				'w' => $image_data[ 1 ],
				'h' => $image_data[ 2 ],
			);
			$items[] = $item;
		}
	}
	return $items;
}

function factorysnc_get_content_images() {
	$items_content = get_post_meta( get_the_ID(), '_factorysnc_content_images', true );
	return unserialize( $items_content );
}

/* Rendi responsive le immagini e gli iframe dentro the_content */
add_filter( 'the_content', 'factorysnc_add_class_and_attr_to_content_media' );

function factorysnc_add_class_and_attr_to_content_media( $content ) {
	$content = mb_convert_encoding( $content, 'HTML-ENTITIES', "UTF-8" );
	$document = new DOMDocument();
	libxml_use_internal_errors( true );
	if ( $content != "" && $content != null ) {
		$document->loadHTML( utf8_decode( $content ) );
		$imgs = $document->getElementsByTagName( 'img' );
		$img_classes = 'img-fluid my-3 content-img ' . PHOTOSWIPE_ELEMENTS_CLASS_NAME;
		$i = 0;
		foreach ( $imgs as $img ) {
			$existing_class = $img->getAttribute( 'class' );
			// error_log( $existing_class );
			$img->setAttribute( 'class', "$existing_class $img_classes" );
			$img->setAttribute( 'data-index', $i++ );
		}

		$iframes = $document->getElementsByTagName( 'iframe' );
		$iframes_classes = 'embed-responsive-item';
		foreach ( $iframes as $iframe ) {
			$existing_class = $iframe->getAttribute( 'class' );
			$iframe->setAttribute( 'class', "$existing_class $iframes_classes" );
		}

		$html = $document->saveHTML();
	} else {
		$html = "";
	}
	return $html;
}

add_filter( 'embed_oembed_html', 'wrap_oembed_html', 99, 4 );

function wrap_oembed_html( $cached_html, $url, $attr, $post_id ) {
	return '<div class="embed-responsive embed-responsive-16by9 my-4">' . $cached_html . '</div>';
}

add_filter( 'get_custom_logo', 'factorysnc_get_the_custom_logo_custom_class' );
// Filter the output of logo to fix Googles Error about itemprop logo
function factorysnc_get_the_custom_logo_custom_class() {

	$classes = array( 'navbar-brand' );
	$classes = implode( ' ', $classes );

	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$html = sprintf( '<a href="%1$s" class="custom-logo-link %2$s" rel="home" itemprop="url">%3$s</a>',
		esc_url( home_url( '/' ) ),
		$classes,
		wp_get_attachment_image( $custom_logo_id, 'full', false, array(
			'class' => 'custom-logo d-inline-block align-top',
		) )
	);
	return $html;
}

add_filter( 'content_save_pre', 'factorysnc_set_content_images_post_meta', 10, 1 );

function factorysnc_set_content_images_post_meta( $content ) {
	// Process content here
	$post_id = get_the_ID();
	$items = array();
	$meta_key = '_factorysnc_content_images';

	if ( $content != "" && $content != null ) {
		$content_copy = $content;
		$content_copy = mb_convert_encoding( $content_copy, 'HTML-ENTITIES', "UTF-8" );
		$content_copy = stripslashes( $content_copy );
		$document = new DOMDocument();
		libxml_use_internal_errors( true );
		$document->loadHTML( utf8_decode( $content_copy ) );
		$imgs = $document->getElementsByTagName( 'img' );

		foreach ( $imgs as $img ) {

			/*if ($img->hasAttributes()) {
				$attributes = $img->attributes;
				// $attributes = stripslashes($img->attributes);
				foreach ($attributes as $attr) {
					$name = $attr->nodeName;
					$value = $attr->nodeValue;
					error_log( "Attribute '$name' :: '$value'".PHP_EOL);
				}
			}*/

			$existing_class = $img->getAttribute( 'class' );
			$existing_class = stripslashes( $existing_class );
			$existing_class = str_replace( '"', "", $existing_class );
			$existing_class = str_replace( "'", "", $existing_class );
			$matches = array();
			$attachment_id = '';

			preg_match( '/no-zoom/', $existing_class, $no_zoom_matches );

			if( count($no_zoom_matches) < 1 ){
				preg_match( '/wp-image-([0-9]+)/', $existing_class, $matches );

				if ( count( $matches ) >= 2 ) {
					$attachment_id = $matches[ 1 ];
				}

				if ( $attachment_id != '' ) {
					$attachment = wp_get_attachment_image_src( $attachment_id, 'full', false );
					$items[] = array(
						'src' => $attachment[ 0 ],
						'w' => $attachment[ 1 ],
						'h' => $attachment[ 2 ],
					);

				} else {
					if($src != “” && $src != null){
						$src = $img->getAttribute( 'src' );
						$src = stripslashes( $src );
						$src = str_replace( '"', "", $src );
						$src = str_replace( "'", "", $src );
						list( $width, $height, $type, $attr ) = getimagesize( $src );

						$items[] = array(
							'src' => $src,
							'w' => $width,
							'h' => $height,
						);
					}
				}
				// error_log($attachment_id);
			}
			// error_log(print_r($no_zoom_matches , true));
		}
		$html = $document->saveHTML();
	}

	update_post_meta( $post_id, $meta_key, serialize( $items ), '' );
	return $content;
}

function opengraph() {
	global $post;
	echo '<meta property="og:image" content="' . get_the_post_thumbnail_url($post, 'full') . '"/>';
	echo '<meta name="twitter:card" content="summary_large_image">';
	echo '<meta name="twitter:image" content="' . get_the_post_thumbnail_url($post, 'full') . '">';
}
add_action('wp_head', 'opengraph', 5);
// function bartag_func( $atts ) {
// 	$atts = shortcode_atts(
// 		array(
// 			'foo' => 'no foo',
// 			'bar' => 'default bar',
// 		), $atts, 'bartag' );

// 	return 'bartag: ' . $atts['foo'] . ' ' . $atts['bar'];
// }
// add_shortcode( 'bartag', 'bartag_func' );

// function factorysnc_banner_with_text_shortcode( $atts, $content = null ) {
// 	// ob_start();
// 	// $html = ob_get_clean();
// 	return '<section class="factorysnc_banner_with_text">' . $content . '</section>';
// }
// add_shortcode( 'factorysnc_banner_with_text', 'factorysnc_banner_with_text_shortcode' );

function factorysnc_banner_with_text_callback( $atts , $content = null) {
	$atts = extract(
		shortcode_atts(
			array(
				'attachment_id' => '',
				'size' => 'normal',
				'font_color' => '',
				'bg_color' => '',
			), $atts, 'factorysnc_banner_with_text'
		)
	);

	$section_classes = "factorysnc_banner_with_text p-4 p-sm-5 row $size";
	$lightbox_classes = "lightbox col-12 col-md-11 col-lg-9";

	$section_css = '';
	$lightbox_css = '';



	if($attachment_id != ''){
		$section_classes .= ' bg-img';
		$section_css .= 'background-image: url('.wp_get_attachment_image_src( esc_attr($attachment_id), 'full', false )[0].');';

		if($bg_color != ''){
			$lightbox_css = 'background-color: '.esc_attr($bg_color).';';
		}
	}

	if($size == 'normal'){
		$section_classes .= ' pt-md-6 pb-md-6';
	}

	if($font_color != ''){
		$lightbox_css .= ' color: '.esc_attr($font_color).';';
	}

	if($bg_color != '' && $attachment_id == ''){
		$section_classes .= ' bg-color';
		$section_css .= 'background-color: '.esc_attr($bg_color).';';
	}else{
		$lightbox_classes .= ' p-5 p-md-6';
	}

	?>
	<?php ob_start();?>
	<section class="<?php echo $section_classes;?>" style="<?php echo $section_css;?>">
		<div class="<?php echo $lightbox_classes;?>" style="<?php echo $lightbox_css;?>">
			<?php echo do_shortcode($content); ?>
		</div>
	</section>
	<?php $html = ob_get_clean();
	return $html;
}
add_shortcode( 'factorysnc_banner_with_text', 'factorysnc_banner_with_text_callback' );

add_filter( 'the_content', 'remove_empty_p', 20, 1 );
function remove_empty_p( $content ){
	// clean up p tags around block elements
	$content = preg_replace( array(
		'#<p>\s*<(div|aside|section|article|header|footer|img)#',
		'#</(div|aside|section|article|header|footer|img)>\s*</p>#',
		'#</(div|aside|section|article|header|footer|img)>\s*<br ?/?>#',
		'#<(div|aside|section|article|header|footer|img)(.*?)>\s*</p>#',
		'#<p>\s*</(div|aside|section|article|header|footer|img)#',
	), array(
		'<$1',
		'</$1>',
		'</$1>',
		'<$1$2>',
		'</$1',
	), $content );
	// return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)*(\s|&nbsp;)*</p>#i', '', $content);
	return preg_replace('#<p>(\s)*+(<br\s*/*>)*(\s)*</p>#i', '', $content);
}