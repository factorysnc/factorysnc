<?php wp_nonce_field( 'factorysnc_banner_with_text_meta_box', 'factorysnc_banner_with_text_meta_box_nonce' );?>
<div class="firstSection">
	<p>
		<a class="factorysnc_select_banner_with_text" href="#">
			<span>Seleziona un immagine</span>
		</a>
		<p>oppure</p>
		<a class="select_color" href="#">
			<span>Seleziona un colore</span>
		</a>
	</p>
</div>
<div class="secondSection" style="display: none;">
	<div class="img">
		<a class="factorysnc_select_banner_with_text" href="#">
			<img src="">
		</a>
		<p><em>Fai clic sull’immagine per modificare o aggiornare</em></p>
		<p><a class="factorysnc_delete_banner_with_text" href="#">Rimuovi il banner</a></p>
		<p><span>Colore Testo: </span><input class="font-color-picker" type="color" value="#be1621"></p>
		<p><span>Colore Lightbox: </span><input class="bg-color-picker" type="color" value="#ffffff"></p>
		
	</div>
	
	<div class="color">
		<p><span>Colore Testo: </span><input class="font-color-picker" type="color" value="#ffffff"></p>
		<p><span>Colore Banner: </span><input class="bg-color-picker" type="color" value="#be1621"></p>
		<p><a class="factorysnc_delete_banner_with_text" href="#">Rimuovi il banner</a></p>
	</div>
	<p>
		<span>Dimensione: </span>
		<select class="size">
			<option value="normal">Normale</option>
			<option value="big">Grande</option>
		</select>
	</p>
	<p><span>Shortcode: </span><input class="shortcode" id="factorysnc_banner_with_text_shortcode" type="text" readonly></input></p>
	<p><button id="factorysnc_banner_with_text_copy_to_clipboard">Copia</button></p>
</div>


<style type="text/css">
#factorysnc_banner_with_text_meta_box *{
	box-shadow: none;
}
</style>