<?php wp_nonce_field( 'custom_wordpress_media_library_meta_box', 'custom_wordpress_media_library_meta_box_nonce' );?>
<div class="images-upload">
	<input type="hidden"  style="width: 100%"  id="gallery-images" name="gallery_images"  value="<?php echo $meta_images; ?>">
	<div style="margin-top: 12px;">
		<button id="galleryImages" class="wp-core-ui button button-small">Aggiungi immagini</button>
		<button id="deleteAllImages" class="wp-core-ui button button-small">Elimina tutte</button><br>
	</div>
	<div class="images-bar-container bar-container" style="display:none">
		<img class="factory-images-loading" src="<?php echo site_url() . '/wp-content/plugins/factory-gallery-zoom/assets/images/admin-loader.svg'; ?>" style="position: relative;width: 50px;margin: 20px auto">
	</div><br>
	
	<?php
	function addSelectedTag($value, $field_name){
		if($value == $field_name){
			echo 'selected="selected"';
		}
	}
	?>
	
	<ul style="display:none;width:100%;" id="sortable" class="connectedList">
		<?php if($meta_images != ''): ?>
			<?php $images_arr = explode(',', $meta_images) ?>
			<?php foreach ($images_arr as $image) : ?>
				<?php $image_data = wp_get_attachment_image_src($image, 'thumbnail' ); ?>
				<li style="display:inline-block;" class="sortableList-item">
					<button class="delete-single-image wp-core-ui button button-small" style="position: absolute;top: 0px;left: 0px;font-size: small;background: white;color: black;border: none;" data-name="<?php echo $image ?>">X</button>
					<img src="<?php echo $image_data[0] ?>" style="width:150px;height:150px;object-fit:cover" data-id="<?php echo $image ?>"/>
				</li>
			<?php endforeach ?>
		<?php endif ?>
	</ul>
	<small id="info-drag">Trascina le immagini per ordinarle.</small>
	<div class="images-errors"></div>
</div>