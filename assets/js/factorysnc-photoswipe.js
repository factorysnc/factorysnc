jQuery(document).ready(function($){
	var	options;
	var gallery;
	var pswpElement = document.querySelectorAll('.pswp')[0];

	if (typeof photoswipe_data !== 'undefined'){
		$(photoswipe_data.photoswipe_elements_selector+':not(.no-zoom)').click(function(){
			options = {
				index: parseInt($(this).data('index')),
				fullscreenEl: false,
				shareEl: false,
				zoomEl: false,
				getThumbBoundsFn: function(index) {
					var thumbnail = document.querySelectorAll(photoswipe_data.photoswipe_elements_selector)[index];
					var pageYScroll = window.pageYOffset || document.documentElement.scrollTop; 
					var rect = thumbnail.getBoundingClientRect(); 
					return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
				}
			};
			gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, photoswipe_data.photoswipe_elements, options);
			gallery.init();
		});
	}
});