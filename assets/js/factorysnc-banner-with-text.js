jQuery(document).ready(function($){
	var $selectImage = $('#factorysnc_banner_with_text_meta_box .factorysnc_select_banner_with_text');

	var $selectColor = $('#factorysnc_banner_with_text_meta_box .select_color');

	var $previewBanner = $('#factorysnc_banner_with_text_meta_box .factorysnc_select_banner_with_text img');
	var $deleteBanner = $('#factorysnc_banner_with_text_meta_box .factorysnc_delete_banner_with_text');
	var $shortCode = $( '#factorysnc_banner_with_text_meta_box .shortcode' );
	var $selectSize = $('#factorysnc_banner_with_text_meta_box .size');
	var $fontColorPicker = $('#factorysnc_banner_with_text_meta_box .font-color-picker');
	var $bgColorPicker = $('#factorysnc_banner_with_text_meta_box .bg-color-picker');

	var $firstSection = $('#factorysnc_banner_with_text_meta_box .firstSection');
	var $secondSection = $('#factorysnc_banner_with_text_meta_box .secondSection');

	var attr_attach = '';
	var attr_size = getSelectedSize();

	var default_bg_color = $bgColorPicker.val();
	var default_font_color = $fontColorPicker.val();

	var attr_bg_color = default_bg_color;

	var attr_font_color = default_font_color;

	var isImg = true;

	var file_frame;


$('#factorysnc_banner_with_text_copy_to_clipboard').click(function(e){
	e.preventDefault();
	$("#factorysnc_banner_with_text_shortcode").select();
	document.execCommand('copy');
});

$selectImage.click(function(e){
	e.preventDefault();
	if ( file_frame ) {
			//file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
			file_frame.open();
			return;
		} else {
			//wp.media.model.settings.post.id = set_to_post_id;
		}
		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Scegli Immagine',
			button: {
				text: 'Scegli immagine',
			},
			library: {
				type: 'image'
			},
			multiple: false
		});
		// console.log(file_frame);
		file_frame.on( 'select', function() {
			var attachment = file_frame.state().get('selection').first().toJSON();
			$previewBanner.attr( 'src', attachment.url ).css( 'width', '100%' ).show();
			attr_attach = attachment.id;
			
			$firstSection.hide();
			$('.color').hide();
			$('.img').show();
			$secondSection.show();
			isImg = true;
			attr_bg_color = hexToRgbA($bgColorPicker.val(), '.94');
			setShortCode();

		});
		file_frame.open();
	});

$selectColor.click(function(e){
	e.preventDefault();
	$firstSection.hide();
	$secondSection.show();
	$('.color').show();
	$('.img').hide();
	attr_attach = '';
	attr_bg_color = default_font_color;
	attr_font_color = default_bg_color;
	setShortCode();
	isImg = false;
});

$deleteBanner.click(function(e){
	e.preventDefault();
	attr_attach = '';
	attr_bg_color = default_bg_color;
	attr_font_color = default_font_color;
	$previewBanner.attr('src' , '');
	$shortCode.val('');
	$firstSection.show();
	$secondSection.hide();
});

$bgColorPicker.on('change', function(){
	if(isImg){
		attr_bg_color = hexToRgbA(this.value, '.94');
		// console.log(attr_bg_color);
	}else{
		attr_bg_color = this.value;
	}

	setShortCode();
		// console.log(this.value);
	});

$fontColorPicker.on('change' , function(){
	attr_font_color = this.value;
	setShortCode();
		// console.log(this.value);
	});

$selectSize.on('change' , function(){
	attr_size = this.value;
	setShortCode();
		// console.log(this.value);
	});

function getSelectedSize(){
	return $selectSize.find('option:selected').attr('value');
}

function setShortCode(){
	$shortCode.val( '[factorysnc_banner_with_text attachment_id="'+attr_attach+'" bg_color="'+attr_bg_color+'" size="'+attr_size+'" font_color="'+attr_font_color+'"] ... [/factorysnc_banner_with_text]' );
}

	// $copyToClipboard.click(function(e){
	// 	e.preventDefault();
	// 	copyToClipboard($shortCode);
	// });

	function hexToRgbA(hex , alpha){
		var c;
		if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
			c= hex.substring(1).split('');
			if(c.length== 3){
				c= [c[0], c[0], c[1], c[1], c[2], c[2]];
			}
			c= '0x'+c.join('');
			return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+alpha+')';
		}
		throw new Error('Bad Hex');
	}
});