var ajax_requests = 0;
jQuery(document).ready(function($){
	'use strict';
	var file_frame;
	var $parent;

	checkIfThereIsAnyImage();
	$('.upload-thumb').on('click',function (e){
		$parent = $(this).parent();
		e.preventDefault();
		if ( file_frame ) {
			//file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
			file_frame.open();
			return;
		} else {
			//wp.media.model.settings.post.id = set_to_post_id;
		}
		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Seleziona Banner',
			button: {
				text: 'Scegli immagine',
			},
			library: {
				type: 'image'
			},
			multiple: false
		});
		console.log(file_frame);
		file_frame.on( 'select', function() {
			console.log($parent);
			var attachment = file_frame.state().get('selection').first().toJSON();
			console.log(attachment);
			$parent.find('img').attr( 'src', attachment.url ).css( 'width', '100%' ).fadeIn();
			$parent.find('.upload-thumb').text('Sostituisci immagine');
			$( '#thumb' ).val( attachment.id );
			//wp.media.model.settings.post.id = wp_media_post_id;
		});
		file_frame.open();
	});
	
	$('#galleryImages').on('click',function (e){
		$parent = $(this).parent();
		e.preventDefault();
		if ( file_frame ) {
			//file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
			file_frame.open();
			return;
		} else {
			//wp.media.model.settings.post.id = set_to_post_id;
		}
		file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Galleria immagini',
			button: {
				text: 'Scegli immagine',
			},
			library: {
				type: 'image'
			},
			multiple: true
		});
		console.log(file_frame);
		file_frame.on( 'select', function() {
			console.log($parent);
			var attachments = file_frame.state().get('selection').toJSON();
			var names = $('#gallery-images').val();
			attachments.forEach(function(attachment){
				$('#sortable').append('<li><button class="delete-single-image wp-core-ui button button-small" style="position: absolute;top: 0px;left: 0px;font-size: small;background: white;color: black;border: none;" data-name="'+attachment.id+'">X</button><img src="' + attachment.url + '" style="width:100%;height: 100%;object-fit: cover;" /></li>');
				if(names == ''){
					names = attachment.id;
				}else{
					names = names + ',' + attachment.id;
				}
				$('#gallery-images').val(names);
			});
			checkIfThereIsAnyImage();
			//wp.media.model.settings.post.id = wp_media_post_id;
		});
		file_frame.open();
	});
	
	$(document).on('click', '.gallery_image', function(){
		var pswpElement = document.querySelectorAll('.pswp')[0];
		var startIndex = parseInt($(this).attr('data-index'));
		var options = {
			index: startIndex,
		};
		var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, photoswipe_parameters.items , options);
		gallery.init();
	});

	$(document).on('click', '.delete-single-image', function(){
		var filename = $(this).attr('data-path');
		var el = $(this);
		el.parent().remove();
		var new_val_raw = $('#gallery-images').val().replace(el.attr('data-name'), '');
		var first_char = new_val_raw.charAt(0);
		var last_char = new_val_raw.substr(new_val_raw.length - 1);
		var new_val = '';
		if(first_char == ','){
			new_val = new_val_raw.substr(1);
		}else if(last_char == ','){
			new_val = new_val_raw.substr(0, new_val_raw.length - 1);
		}else{
			new_val = new_val_raw.replace(',,', ',');
		}
		$('#gallery-images').val(new_val);
		checkIfThereIsAnyImage();
	});

	$('.add-link-btn').click(function(e){
		e.preventDefault();
		var last_id = 0;
		var last_id_attr = $('.video-links div:last').attr('data-id');
		if(typeof last_id_attr === typeof undefined || last_id_attr === false){
			last_id = 0;
		}else{
			last_id = parseInt(last_id_attr) + 1;
		}
		$('.video-links').append('<div class="single-gallery-video-link" data-id="' + last_id + '">' + 
			'<label for="single-video-' + last_id + '">Link Video Gallery :</label><input style="width: 60%;position: relative;left: 10px;" type="text" id="single-video-' + last_id + '" name="gallery-videos[]">' +
			'<button class="wp-core-ui button button-small remove-single-link-btn" style="position:absolute;right:22%">X</button>' +
			'</div>');
	});

	$(document).on('click', '.remove-single-link-btn', function(e){
		e.preventDefault();
		$(this).parent().remove();
	});

	$('#deleteAllImages').click(function(e){
		e.preventDefault();
		$('#sortable').empty();
		$('#gallery-images').val('');
		$('.images-errors').html('');
		checkIfThereIsAnyImage();
	});

	$('#galleryImages').on('click', function(){
		$('#uploadImages').show();
		$('.images-errors').html('');
	});

	$('#uploadThumbLink').click(function(e){
		e.preventDefault();
		$('#factoryThumbFile').trigger('click');
	});

	$('#factoryThumbFile').on('change', function(){
		if($(this).val() != ''){
			$('#uploadThumb').trigger('click');
			$('.factory-thumb-loading').show();
			$('#new-thumb').hide();
		}
	});

	$('.remove-th').click(function(e){
		e.preventDefault();
		$('#new-thumb img').remove();
		$('#thumb').val('');
		$('.remove-th').hide();
	});

	function checkIfThereIsAnyImage(){
		if($('ul#sortable li').length != 0){
			$('#deleteAllImages , #info-drag').show();
		}else{
			$('#deleteAllImages , #info-drag').hide();
		}
	}

});

jQuery(function($){
	'use strict';
	$( "#sortable" ).disableSelection();

	$('#sortable').imagesLoaded(function(){
		$('#sortable').fadeIn();
		$( "#sortable" ).sortable({
			update:function(){
				$('#gallery-images').val('');
				$('#sortable').find('img').each(function(){
					var name = $(this).data('id');
					if($('#gallery-images').val() == ''){
						$('#gallery-images').val(name);
					}else{
						$('#gallery-images').val($('#gallery-images').val() + ',' + name);
					}
				});
			}
		});
	});
});
